"""
Should be compiled with cython --embed

This is an attempt at a faster consistency audit for swift storage devices.
It uses XFS magic ioctls to bulkstat.
"""

import sys
import os
from optparse import OptionParser

from stdlib cimport malloc, free

from swift.obj.server import read_metadata
from swift.common.ring import Ring
from swift.common.utils import storage_directory, hash_path


AT_A_TIME = 250000


cdef extern from 'sys/ioctl.h':
    int ioctl(int d, int request, ...)

cdef extern from 'xfs/xfs.h':
    struct xfs_bstat:
        int bs_mode
        int bs_size
        int bs_xflags
    struct xfs_fsop_bulkreq:
        unsigned long long int *lastip
        int icount
        xfs_bstat *ubuffer
        int *ocount
    int XFS_XFLAG_HASATTR, XFS_IOC_FSBULKSTAT

cdef extern from 'xfs/jdm.h':
    ctypedef void *jdm_filehandle_t
    ctypedef void *jdm_fshandle_t
    jdm_fshandle_t *jdm_getfshandle(char *mntpnt)
    int jdm_open(jdm_fshandle_t *fshandlep, xfs_bstat *sp, int oflags)


def audit_mountpoint(path, at_a_time):
    """
    """
    cdef int count = 0
    cdef unsigned long long int last = 0
    cdef xfs_bstat *t = <xfs_bstat *>malloc(at_a_time * sizeof(xfs_bstat))
    cdef xfs_fsop_bulkreq bsr
    cdef jdm_fshandle_t *fs = jdm_getfshandle(path)
    cdef jdm_filehandle_t *handle
    cdef size_t hlen
    bsr.lastip = &last
    bsr.icount = at_a_time
    bsr.ubuffer = t
    bsr.ocount = &count
    fd = os.open(path, os.O_DIRECTORY)
    object_ring = Ring('/etc/swift/object.ring.gz')
    check_count = 0

    while ioctl(fd, XFS_IOC_FSBULKSTAT, &bsr) == 0 and bsr.ocount[0] > 0:
        for i in xrange(bsr.ocount[0]):
            if t[i].bs_size:
                continue
            if t[i].bs_xflags & XFS_XFLAG_HASATTR:
                check_count += 1
                file_fd = jdm_open(fs, &t[i], os.O_RDONLY)
                metadata = read_metadata(file_fd)
                if 'name' in metadata and 'Content-Length' in metadata and \
                        int(metadata['Content-Length']) != t[i].bs_size:
                    _junk, account, container, obj = metadata['name'].split('/', 3)
                    partition, nodes = object_ring.get_nodes(account, container, obj)
                    name_hash = hash_path(account, container, obj)
                    corrupt_hash = os.path.join(
                        path, storage_directory('objects', partition, name_hash))
                    print "CORRUPTED:", corrupt_hash
                os.close(file_fd)
    free(t)
    os.close(fd)
    print "Files checked:", check_count


if __name__ == '__main__':
    parser = OptionParser(usage=
                "Usage: %prog [options] [mountpoint [mountpoint [...]]]")
    options, args = parser.parse_args()
    if not args:
        parser.error("Required mountpoint argument missing.")
    for arg in args:
        audit_mountpoint(arg, AT_A_TIME)
