swift_bulkaudit: swift_bulkaudit.pyx
	cython --embed swift_bulkaudit.pyx
	gcc swift_bulkaudit.c -O3 -fPIC -o swift_bulkaudit -lpython2.6 -I/usr/include/python2.6 -lhandle
